/*
Задание:

Написать конструктор который генерирует слайдер с теми настройками которые вы туда передали.
Выводит слайдер на страницу. Можно для слайдера использовать заготовку которую делали как ДЗ
на первых занятиях.

new MySlider( params );

*/

window.addEventListener("load", function () {

    var divs = document.getElementsByClassName('load');
    [].forEach.call(divs, function (elem) {
        elem.style.backgroundColor = randomColor();
        var number = Math.floor((Math.random() * 5) + 1);
        var content = new ColorsContainer(number);
        elem.appendChild(content);
     });

    function ColorsContainer(colorsNum) {

        var colorsContent = document.createElement('div');
        colorsContent.className = 'colorsContent';

        for(var i = 0; i < colorsNum; i++){
            var color = document.createElement('div');
            color.className = 'selectColor';
            color.style.backgroundColor = randomColor();
            colorsContent.appendChild(color);
        };

        var position = confirm('Make it horizontal?');

        position ? (colorsContent.style.width = (colorsNum*134)+'px') : colorsContent.style.height = (colorsNum*134)+'px';

        return colorsContent;

    };

    function randomColor() {
        return "#" + Math.random().toString(16).slice(2, 8);
    };

});